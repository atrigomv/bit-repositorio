#!/usr/bin/python3

import urllib3
import json
import argparse
import sys
import time

#	(ADMIN) 'Authorization': 'Token 1805832d96a56d08cedd1edf9dc4dcd130e3d2a1'}
#       (Jenkins) 'Authorization': 'Token cd4d7dadc3633fd67cd90a48217fd5a3c36a76c1'}

#	Managing the paremeters

parser = argparse.ArgumentParser()
parser.add_argument('-k', '--key', help='Valid Authorization Token')
parser.add_argument('-p', '--product', help='Defect Dojo ProductID')
parser.add_argument('-i', '--ip', help='Defect Dojo IP server')
parser.add_argument('-v', '--environment', help='Test environment: Default, Development, Lab, Pre-prod, Production, Staging, Test (OPTIONAL)')
parser.add_argument('-e', '--engagement', help='Name of the engagement (OPTIONAL)')
parser.add_argument('-m', '--minimum', help='Minimum severity in the import of findings: Info, Low, Medium, High, Critical (OPTIONAL)')

args = parser.parse_args()

if((args.key is None) or (args.product is None) or (args.ip is None)):
	if(args.key is None):
		print('[ERROR] A valid Authorization Token need to be passed through "k" parameter. Example: ' + sys.argv[0] + ' -k XYXYXYXYXYXYXYXYX - p test-product -i 127.0.0.1')
	if(args.product is None):
		print('[ERROR] A valid Product Defect Dojo name need to be passed through "p" parameter. Example: ' + sys.argv[0] + ' -k XYXYXYXYXYXYXYXYX - p test-product -i 127.0.0.1')
	if(args.ip is None):
		print('[ERROR] A valid IP or domain name of Defect Dojo server need to be passed through "i" parameter. Example: ' + sys.argv[0] + ' -k XYXYXYXYXYXYXYXYX - p test-product -i 127.0.0.1')
	sys.exit()

if(args.engagement is None):
        args.engagement = 'REV_' + time.strftime('%Y%m') + '_' + str(args.product) + '_' + time.strftime('%d%H%M%S')

if(args.environment is None):
	args.environment = 'Default'

if(args.minimum is None):
	args.minimum = 'Low'

#	Initializing pool manager

http = urllib3.PoolManager()

#	Retrieve the 'product ID through the name (is not case sensitive)'

url = 'http://' + str(args.ip) + ':8080/api/v2/products/?name=' + str(args.product)

headers = {'Content-Type': 'application/json',
           'Authorization': 'Token ' + args.key}

resp = http.request('GET', url, headers=headers)

s = json.loads(resp.data)

if(s['count'] == 0):
	print('[ERROR] Product name "' + args.product + '" not found.')
	sys.exit()
else:
	productID = s['results'][0]['id']


#	Create new engagement

url = 'http://' + str(args.ip) + ':8080/api/v2/engagements/'

headers = {'Content-Type': 'application/json',
           'Authorization': 'Token ' + args.key}

body = json.dumps({'name': args.engagement,
      'description': 'Engagement automatic generated through API v2. Product: ' + args.product,
      'target_start': time.strftime('%Y-%m-%d'),
      'target_end': time.strftime('%Y-%m-%d'),
      'status': 'Completed',
      'engagement_type': 'CI/CD',
      'product': productID})

resp = http.request('POST', url, headers=headers, body=body)

s = json.loads(resp.data)

engagementID = s['id']

#	Create new test

url = 'http://' + str(args.ip) + ':8080/api/v2/tests/'

headers = {'Content-Type': 'application/json',
           'Authorization': 'Token ' + args.key}

body = json.dumps({'engagement': engagementID,
      'title': 'Prisma Cloud Test',
      'target_start': time.strftime('%Y-%m-%dT%H:%M:%S.312Z'),
      'target_end': time.strftime('%Y-%m-%dT%H:%M:%S.312Z'),
      'test_type': 2})	# 1: API Test; 2: Static Check

resp = http.request('POST', url, headers=headers, body=body)

s = json.loads(resp.data)

testID = s['id']

#	Create findings

f = open('prisma-cloud-scan-results.json')
data = json.load(f)

url = 'http://' + str(args.ip) + ':8080/api/v2/findings/'
headers = {'Content-Type': 'application/json',
	   'Authorization': 'Token ' + args.key}

for i in data[0]['entityInfo']['vulnerabilities']:
        #print(i['cve'] + ', ' + i['severity'] + ', ' + i['description'] + '\n')
	body = json.dumps({'test': testID,
			   'found_by': [4],
			   'url': i['link'],
			   'title': i['cve'],
			   'cve': i['cve'],
			   'cvssv3': i['vecStr'],
			   'cvssv3_score': i['cvss'],
			   'severity': i['severity'][0].upper() + i['severity'][1:],
			   'description': i['description'],
			   'mitigation': i['status'],
			   'active': True,
			   'verified': False,
			   'false_p': False,
			   'duplicate': False,
			   'numerical_severity': i['cvss'],
			   'component_name': i['packageName'],
			   'component_version': i['packageVersion']})

	resp = http.request('POST', url, headers=headers, body=body)
	s = json.loads(resp.data)

	print(s)