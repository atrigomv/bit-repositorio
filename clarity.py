import re, os
#import logging
import urllib3
import configparser
import sys
import time

from lxml import etree
from requests import Session
#from requests_ntlm import HttpNtlmAuth
from zeep import Client, Settings, xsd
#from zeep.cache import SqliteCache
from zeep.transports import Transport
#from zeep.plugins import HistoryPlugin

##### CONFIGURACION INICIAL SCRIPT #####

config_file = 'C:/Users/ATRIGOM/Desktop/Trigonometron/clarity.ini'
error_file = 'C:/Users/ATRIGOM/Desktop/Trigonometron/error.log'
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
flagA = 0
flagB = 0
cont_proyectos = 0
cont_proyectos_skip = 0
cont_proyectos_error = 0

### Configuracion de calidad ####################################################
#class TransportMAPFRE(Transport):
#    def post_xml(self, address, envelope, headers):
#        #message = etree_to_string(envelope)
#        message = etree.tostring(envelope).decode("utf-8")
#        message = re.sub(r"(</?soap-env:Body>)\s*\1", "\\g<1>", message)
#        return self.post(address, message, headers)
#################################################################################

##### RECUPERACION DE INFO DEL FICHERO DE CONFIGURACION #####

try:
    print('Retrieving config info from ini file...')
    parser = configparser.ConfigParser()
    parser.read(config_file)
    
    wsdl = parser.get('settings', 'wsdl')
    username = parser.get('settings', 'username')
    password = parser.get('settings', 'password')
    tenant = parser.get('settings', 'tenant')
except:
    print('ERROR: there is no config file in the location (or is not well formed): ' + config_file)
    f = open(error_file, 'a')
    f.write(str(time.ctime(time.time())) + ',ERROR opening config file\n')
    f.close()
    sys.exit(0)

##### CREACION DEL CLIENTE #####

session = Session()
session.verify = False  # Verificacion TLS quitada

### Configuracion de calidad ####################################################
#cache = SqliteCache(timeout=3000)
#transport = TransportMAPFRE(cache=cache, session=session)
#history = HistoryPlugin()
#client = Client(wsdl, transport=transport, plugins=[history], settings=settings)
#################################################################################

### Configuracion DISMA #########################################################
transport = Transport(session=session) # Te permite configurar temas de red. Lo usamos para quitar verificacion TLS
settings = Settings(strict=False, xml_huge_tree=True) 
client = Client(wsdl, transport=transport, settings=settings)

service = client.service

##### AUTENTICACION DEL CLIENTE #####
SessionID = service.Login(Username=username, Password=password, TenantID=tenant)

##### LLAMADA AL SERVICIO DE QUERY #####

header = {'SessionID': SessionID}
factoryns0 = client.type_factory('ns0')
body = factoryns0.map_quality_pki_viewQuery()
body['Code'] = 'map_quality_pki_view'
body['Filter'] = factoryns0.map_quality_pki_viewFilter()
#body['Filter']['id_proyecto'] = '00414240'

print('Retrieving information from Clarity...')
result = service.Query(header=xsd.SkipValue, _soapheaders={'header': header}, body=body)


for record in result.Records.Record:
    try:
        proyecto = str(record.proyecto)
    except:
        flagA = 1
    try:
        jp = str(record.jefe_proyecto)
    except:
        if(flagB == 0):
            cont_proyectos_error = cont_proyectos_error + 1
            flagB = 1
        jp = 'Error info'
    try:
        objetivos = str(record.objetivos)
    except:
        if(flagB == 0):
            cont_proyectos_error = cont_proyectos_error + 1
            flagB = 1
        objetivos = 'Error info'
    if(flagA == 0):
        cont_proyectos = cont_proyectos + 1
        print('PROYECTO: ' + proyecto + '   JP: ' + jp + '   OBJETIVOS: ' + objetivos)
    else:
        cont_proyectos_skip = cont_proyectos_skip + 1
        flagA = 0
    flagB = 0
print(str(cont_proyectos) + ' projects retrieved. ' + str(cont_proyectos_skip) + ' projects skipped. ' + str(cont_proyectos_error) + ' projects with some errors ("Error info")')
